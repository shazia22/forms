<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <?php include('config.php') ;

?>
    <head>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style>
      body { background-color: white; }
 #detailsForm { 
     background-color: burlywood;
           } 
  
 </style>
    </head>
    <body>
        <?php
        if ($db->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
else{
//echo "Connected successfully";
}
            ?>
        <div align="center">
    <div id="detailsForm" style="width: 400px; height: 600px;">
  <form method="post" action="config.php" id="contactForm" name="register" role="form">
    <div class="container">
    <h3>Details Form</h3>
    <p>Please fill in this form to create an account.</p>
    <hr>
    <div class="form-row">
       <label for="email"><b>Email</b></label>
    <input type="text" class="form-control" name="email" id="email_ID">
   
     <label for="password"><b>Password</b></label>
     <input type="password" class="form-control" name="password" id="passwordID"> 
        
    <label for="firstname"><b>First Name</b></label>
    <input type="text" class="form-control" name="first_name"  id="first_nameID">

    <label for="lastname"><b>Last Name</b></label>
    <input type="text" class="form-control" name="last_name" id="last_nameID">

     
    
    <label for="address"><b>Address</b></label>
    <textarea class="form-control" name="address" id="address_ID"></textarea>
    
    <label for="phoneno"><b>Phone No</b></label>
    <input type="text" class="form-control" name="phone_no" id="phone_noID">  
    <hr>
        <button type="submit" name="reg_user" class="btn btn-primary">Register</button>

  </div><br/>

    </div>
<!--  <div class="container signin">
    <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>-->
</form>  
    </div>
    
  </div>
    </body>
</html>


<!--<!DOCTYPE html>
<html>
<head>
        <title>Cricket Game</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style>
      body { background-color: white; }
/*      canvas { background-color: #222222; }*/
 #game-canvas {
/*            display: none; */

           }
           #detailsForm {
     background-color: burlywood;
 display: none;

           }
 </style>
</head>
<body onload="firstPage();">

    <div align="center">
    <div id="game-canvas" style="width:400px; height:600px;">
     </div>

  </div>
    <script type="text/javascript" src="Pixi/pixi.min.js"></script>

   <script>

//Create a Pixi Application

    let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite;
    let Text=PIXI.Text;
    let TextStyle=PIXI.TextStyle;
//    let Button=PIXI.UI.Button;
        imageLoader = PIXI.loader;
    resourcesImage = PIXI.loader.resources;
//let loadSound = PIXI.sound;
  PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.LINEAR;
       var gameLoopStarted=false;
//const sound = PIXI.sound;

let style = new TextStyle({
  fontFamily: "Arial",
  fontSize: 36,
  fill: "white",
  stroke: '#ff3300',
  strokeThickness: 4,
  dropShadow: true,
  dropShadowColor: "#000000",
  dropShadowBlur: 4,
  dropShadowAngle: Math.PI / 6,
  dropShadowDistance: 6,
});

let app = new PIXI.Application({
    width: 400,         // default: 800
    height: 600,        // default: 600
    antialias: true,    // default: false
    transparent: false, // default: false
    resolution: 1,       // default: 1
//    resolution: window.devicePixelRatio || 1
  }
);

app.renderer.autoResize = true;
app.renderer.backgroundColor = 0x061639;
 PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;


var canvas= document.querySelector("#game-canvas").appendChild(app.view);

var buttonaudio = new Audio('public_html/sounds/startbuttonSound.mp3');

var gameSound= new Audio('public_html/sounds/sport.mp3');
       gameSound.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);
  var soundEffects=true;
var animatedIdlebowler, animatedSwingbowler,animatedStump,animatedIdlebatMan,animatedSwingbatMan,ball,two,four,six,stumpBottom;
       var emitter;
         var matchScreen;
         var bowlerIdleAnimation,bowlerSwingAnimation;
           var batManIdleAnimation,batManSwingAnimation,ballHitAnimation,animatedBallHit,victoryExplodeAnimation,animatedVictoryExplosion,blastAnimation,lightAnimation,croudAnimation;
   var clickCount=0;
   
   var popupPanel;
    function firstPage(){

  loader
  //First Screen sprites
        .add("public_html/images/streetcriccketTextImg.png")
        .add("public_html/images/FirstScreenBgImg.png")
        .add("public_html/images/Buttons/StartButtonFirstScreen.png")
        .add("public_html/images/Buttons/StartButtonDownFirstScreen.png")
        .add("public_html/images/Buttons/StartButtonHoverFirstScreen.png")
//End first screen Sprites

//second screen sprites
.add("public_html/images/Buttons/FullScreenButton.png")
.add("public_html/images/Buttons/FullScreenButtonDown.png")
.add("public_html/images/Buttons/FullScreenButtonHover.png")
.add("public_html/images/sky.png")
.add("public_html/images/cloud 1.png")
.add("public_html/images/cloud 2.png")


//end second screen sprite

.add("public_html/images/boards.png")
.add("public_html/images/whitebg.jpg")

.add("public_html/images/board-with-shadow.png")
.add("public_html/images/stumps.png")
.add("public_html/images/victory-scrn-white-board.png")
.add("public_html/images/play again button.png")


.add("public_html/images/Buttons/SoundOnButton.png")
.add("public_html/images/Buttons/SoundOnButtonDown.png")
.add("public_html/images/Buttons/SoundOnButtonHover.png")

.add("public_html/images/Buttons/SoundOffButton.png")
.add("public_html/images/Buttons/SoundOffButtonDown.png")
.add("public_html/images/Buttons/SoundOffButtonHover.png")

.add("public_html/images/Buttons/MusicOffButton.png")
.add("public_html/images/Buttons/MusicOffButtonDown.png")
.add("public_html/images/Buttons/MusicOffButtonHover.png")



.add("public_html/images/Buttons/MusicOnButton.png")
.add("public_html/images/Buttons/MusicOnButtonDown.png")
.add("public_html/images/Buttons/MusicOnButtonHover.png")

.add("public_html/images/Buttons/RefreshButton.png")
.add("public_html/images/Buttons/RefreshButtonDown.png")
.add("public_html/images/Buttons/RefreshButtonHover.png")

        .add("public_html/images/Buttons/play-btn.png")
        .add("public_html/images/Buttons/play-btnPressed.png")
        .add("public_html/images/blackbg.png")
        .add("public_html/images/Gorundwithpitch.png")
        .add("public_html/images/stump.png")
        .add("public_html/images/arrow.png")
        .add("public_html/images/YippeeScreen.png")
        .add("public_html/images/YippeeScreenpanelNew.png")
        .add("public_html/images/tryagainbtn.png")
        .add("public_html/images/dhoni.gif")
        .add("public_html/images/Buttons/StartButton.png")
        .add("public_html/images/Buttons/StartButtonDown.png")
        .add("public_html/images/Buttons/StartButtonHover.png")

        .add("public_html/images/bowler.png")
        .add("public_html/images/stadiumWithGround2.png")
        .add("public_html/images/scoreboardWithout_no.png")

    .add("public_html/images/stump-animation.json")
        .add("public_html/images/batmanIdle/BatsManIdle.json")

//        .add("public_html/images/batmanIdle/batmanIdleNew.json")

    .add("public_html/images/BallHit.json") //New
      .add("public_html/images/Explosion.json")
    .add("public_html/images/batmanSwing/BatsManHit.json") //New
            .add("public_html/images/BowlerSwing/BowlerSwing.json")
    .add("public_html/images/bowlerIdle/BowlerIdle.json")
        .add("public_html/images/progressbarfront.png")
 .add("public_html/images/progressbarbg.png")

.add("public_html/images/Conffeeti_Blast_animation/blastAnimation.json")
.add("public_html/images/croud_animation/croudanimation.json")
 .add("public_html/images/ball.png")
 .add("public_html/images/six_number.png")
 .add("public_html/images/four_number.png")
 .add("public_html/images/two_number.png")
 .add("public_html/images/boxforscorewithtext.png")
 .add("public_html/images/First_screen/MainScreenBg.svg")
  .add("public_html/images/clicktostart.png")
  .add("public_html/images/close_button.png")
.add("public_html/images/stadium_withoud_croud.png")

        .add("public_html/images/light_animation/lightAnimation.json")
        .add("public_html/images/close_buttonDown.png")
        .add("public_html/images/close_buttonHover.png")

        .add("public_html/images/powergaugeText.png")
        .load(setup);

       function setup() {
    ballHitAnimation=PIXI.loader.resources["public_html/images/BallHit.json"].spritesheet;
    victoryExplodeAnimation=PIXI.loader.resources["public_html/images/Explosion.json"].spritesheet;
    batManIdleAnimation = PIXI.loader.resources["public_html/images/batmanIdle/BatsManIdle.json"].spritesheet;

blastAnimation=PIXI.loader.resources["public_html/images/Conffeeti_Blast_animation/blastAnimation.json"].spritesheet;
        
    batManSwingAnimation = PIXI.loader.resources["public_html/images/batmanSwing/BatsManHit.json"].spritesheet;

   bowlerIdleAnimation = PIXI.loader.resources["public_html/images/bowlerIdle/BowlerIdle.json"].spritesheet;
   bowlerSwingAnimation = PIXI.loader.resources["public_html/images/BowlerSwing/BowlerSwing.json"].spritesheet;


croudAnimation=PIXI.loader.resources["public_html/images/croud_animation/croudanimation.json"].spritesheet;

lightAnimation=PIXI.loader.resources["public_html/images/light_animation/lightAnimation.json"].spritesheet;
//canvas background image start
let firstscreenBg=new Sprite(new PIXI.Texture.from("public_html/images/FirstScreenBgImg.png",undefined,undefined,1.0));
//  let firstscreenBg = new Sprite(resources["public_html/images/First_screen/MainScreenBg.svg"].texture);//FirstScreenBgImg
  firstscreenBg=reSetAnchor(firstscreenBg);
 firstscreenBg=setPosition(firstscreenBg,app.renderer.width/2,app.renderer.height/2);
 firstscreenBg.scale.set(1);
  app.stage.addChild(firstscreenBg);
  //canvas bg end

    

  //street Cricket text Image start
  let SCTextImg = new Sprite(resources["public_html/images/streetcriccketTextImg.png"].texture);
  SCTextImg=reSetAnchor(SCTextImg);
  SCTextImg=setPosition(SCTextImg,2,-170);
 SCTextImg.scale.set(0.6);
 firstscreenBg.addChild(SCTextImg);
  //end street Cricket text Image

 //Start First screen start button
var textureButton = resources["public_html/images/Buttons/StartButtonFirstScreen.png"].texture;
var textureButtonDown = resources["public_html/images/Buttons/StartButtonDownFirstScreen.png"].texture;
var textureButtonOver = resources["public_html/images/Buttons/StartButtonHoverFirstScreen.png"].texture;

 var button = new PIXI.Sprite(textureButton);

  button=reSetAnchor(button);
   button=setPosition(button,200,500);
  button.scale.set(0.2);
  app.stage.addChild(button);

    button.interactive = true;
    button.buttonMode = true;

    // button events.
         button.on('pointerup', onClick);
        button.on('pointerdown', onButtonDown);
        button.on('pointerup', onButtonUp);
        button.on('pointerupoutside', onButtonUp);
        button.on('pointerover', onButtonOver);
        button.on('pointerout', onButtonOut);


   function onClick(event){

    if(soundEffects== true)
   buttonaudio.play();

  app.stage.addChild(secondPage());
    }

function onButtonDown() {
    this.isdown = true;
    this.texture = textureButtonDown;
    this.alpha = 1;
}

function onButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = textureButtonOver;
    }
    else {
        this.texture = textureButton;
    }
}

function onButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = textureButtonOver;
}

function onButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = textureButton;
}
 app.ticker.add(delta => gameLoopFirstScreen(delta));


function gameLoopFirstScreen(delta){
firstScreenStartButtonAnimate(delta);
}

//start Button section
var FirstScreenStartButtonScale=0.2;
 var FSminimumScale=0.18;
 var FSmaximumScale=0.22;
  var FSspeedOfStartButtonScale=0.001;
 var FSscaleSwitch=1;

function firstScreenStartButtonAnimate(delta){


 FirstScreenStartButtonScale+= FSscaleSwitch*(FSspeedOfStartButtonScale*delta);
button.scale.set(FirstScreenStartButtonScale);
if(FirstScreenStartButtonScale > FSmaximumScale){
 FSscaleSwitch= -1;

}
else if(FirstScreenStartButtonScale < FSminimumScale){
    FSscaleSwitch=1;
}
}
}
  gameSound.play();

}


    function secondPage(){
  //black bg start

  var secondPageBlackBg = new Sprite(resources["public_html/images/blackbg.png"].texture);

 secondPageBlackBg=reSetAnchor(secondPageBlackBg);
 secondPageBlackBg=setPosition(secondPageBlackBg,app.renderer.width/2,app.renderer.height/2.1);
 secondPageBlackBg.scale.set(1);
 app.stage.addChild(secondPageBlackBg);
  //end black bg

  //sky start
  var skyImg = new Sprite(resources["public_html/images/sky.png"].texture);

 skyImg=reSetAnchor(skyImg);
 skyImg=setPosition(skyImg,app.renderer.width/2,app.renderer.height/8);
 skyImg.scale.set(1);
 app.stage.addChild(skyImg);
  //sky end
 var cloud1 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud1=reSetAnchor(cloud1);
 cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
 cloud1.scale.set(0.1);
 app.stage.addChild(cloud1);

  var cloud2 = new Sprite(resources["public_html/images/cloud 2.png"].texture);
  cloud2=reSetAnchor(cloud2);
 cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
 cloud2.scale.set(0.1);
 app.stage.addChild(cloud2);

 var cloud3 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud3=reSetAnchor(cloud3);
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 cloud3.scale.set(0.1);
 app.stage.addChild(cloud3);
app.ticker.add(delta => gameLoopCloud(delta));

function gameLoopCloud(delta){
//                     console.log(cloud3.position.x);

cloud1.x += 1;
 cloud2.x+=1;
  cloud3.x+=1;

if(cloud1.position.x==456){
cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
}
if(cloud2.position.x==480){
  cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
}
if(cloud3.position.x==480){
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 }


}
 //Start BG screen

//  var secondBgScreen = new Sprite(resources["public_html/images/Gorundwithpitch.png"].texture);
 var secondBgScreen = new Sprite(resources["public_html/images/stadium_withoud_croud.png"].texture);
 secondBgScreen=reSetAnchor(secondBgScreen);
 secondBgScreen=setPosition(secondBgScreen,app.renderer.width/2,app.renderer.height/1.9);
 secondBgScreen.scale.set(1);
 app.stage.addChild(secondBgScreen);
 //End BG Screen

 //light Animation
 animatedlight = new PIXI.AnimatedSprite(lightAnimation.animations["light"]);

     animatedlight=setPosition(animatedlight,2,-200);
    animatedlight.scale.set(0.5);//0.32
    animatedlight.animationSpeed = 0.1;
    animatedlight.play();
    animatedlight.visible=true;
  animatedlight.loop=true;

     secondBgScreen.addChild(animatedlight);
 //End light Animation
 //start Stump section (gif , position , anchor,scale)
   var secondPagestump1Top = new Sprite(resources["public_html/images/stumps.png"].texture);
 secondPagestump1Top=reSetAnchor(secondPagestump1Top);
 secondPagestump1Top=setPosition(secondPagestump1Top,2,-18);
 secondPagestump1Top.scale.set(0.3);
  secondBgScreen.addChild(secondPagestump1Top);

 var secondPagestump1Bottom = new Sprite(resources["public_html/images/stumps.png"].texture);
 secondPagestump1Bottom=reSetAnchor(secondPagestump1Bottom);
 secondPagestump1Bottom=setPosition(secondPagestump1Bottom,2,235);
 secondPagestump1Bottom.scale.set(0.6);
  secondBgScreen.addChild(secondPagestump1Bottom);


  //End Stump section


//street Cricket text Image start
  let SecondPageSCTextImg = new Sprite(resources["public_html/images/streetcriccketTextImg.png"].texture);
  SecondPageSCTextImg=reSetAnchor(SecondPageSCTextImg);
  SecondPageSCTextImg=setPosition(SecondPageSCTextImg,2,-170);
 SecondPageSCTextImg.scale.set(0.5);
 secondBgScreen.addChild(SecondPageSCTextImg);
  //end street Cricket text Image

//Start Third Page center Panel(set image ,anchor,position,scale)
// var popupPanel = PIXI.Sprite.fromImage('public_html/images/YippeeScreenpanelNew.png');

  popupPanel = new Sprite(resources["public_html/images/YippeeScreenpanelNew.png"].texture);
  popupPanel=reSetAnchor(popupPanel);
  popupPanel=setPosition(popupPanel,app.renderer.width/2,app.renderer.height/2);
  popupPanel.scale.set(0.8);
  app.stage.addChild(popupPanel);

  var closeButton = resources["public_html/images/close_button.png"].texture;
   var closeButtonDown = resources["public_html/images/close_buttonDown.png"].texture;
   var closeButtonOver = resources["public_html/images/close_buttonHover.png"].texture;

    var closepopupPanel = new PIXI.Sprite(closeButton);
  closepopupPanel=setPosition(closepopupPanel,180,-285);
 closepopupPanel.scale.set(0.2);

    closepopupPanel=reSetAnchor(closepopupPanel);
    closepopupPanel.interactive = true;
    closepopupPanel.buttonMode = true;
    popupPanel.addChild(closepopupPanel);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    closepopupPanel.on('pointerdown', ButtonDownClostButton);
    closepopupPanel.on('pointerup', ButtonUpClostButton);
    closepopupPanel.on('pointerover', ButtonOverClostButton);
    closepopupPanel.on('pointerout', ButtonOutClostButton);
    closepopupPanel.on('mouseover', ButtonOverClostButton);

   function ButtonDownClostButton() {
    this.isdown = true;
      this.texture = closeButtonDown;
    this.alpha = 1;
app.stage.removeChild(popupPanel);

}
     function ButtonUpClostButton() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = closeButtonOver;
    }
    else {
        this.texture = closeButton;
    }
}

function ButtonOverClostButton() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = closeButtonOver;
}

function ButtonOutClostButton() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = closeButton;
}
  //End Third Page center Panel.


  //start set button image ,position,scale,intractive and mode.
   var textureButton2 = resources["public_html/images/Buttons/StartButton.png"].texture;
   var textureButtonDown2 = resources["public_html/images/Buttons/StartButtonDown.png"].texture;
   var textureButtonOver2 = resources["public_html/images/Buttons/StartButtonHover.png"].texture;

    var button2 = new PIXI.Sprite(textureButton2);
      button2=setPosition(button2,140,240);

    button2=reSetAnchor(button2);
    button2.interactive = true;
    button2.buttonMode = true;
    secondBgScreen.addChild(button2);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    button2.on('pointerdown', onButtonDown2);
    button2.on('pointerup', onButtonUp2);
    button2.on('pointerover', onButtonOver2);
    button2.on('pointerout', onButtonOut2);
    button2.on('mouseover', onButtonOver2);


   function onButtonDown2() {

    this.isdown = true;
      this.texture = textureButtonDown2;
    this.alpha = 1;
    if(soundEffects== true)
    buttonaudio.play();
app.stage.addChild(thirdPage());
}
     function onButtonUp2() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = textureButtonOver2;
    }
    else {
        this.texture = textureButton2;
    }
}

function onButtonOver2() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = textureButtonOver2;
}

function onButtonOut2() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = textureButton2;
}
    //End Button Events

  app.ticker.add(delta => gameLoop(delta));
  

function gameLoop(delta){
startButtonAnimate(delta);
}

//start Button section
var startButtonScale=0.2;
 var minimumScale=0.18;
 var maximumScale=0.22;
  var speedOfStartButtonScale=0.001;
 var scaleSwitch=1;

function startButtonAnimate(delta){
 startButtonScale+= scaleSwitch*(speedOfStartButtonScale*delta);
button2.scale.set(startButtonScale);
closepopupPanel.scale.set(startButtonScale);
if(startButtonScale > maximumScale){
 scaleSwitch= -1;

}
else if(startButtonScale < minimumScale){
    scaleSwitch=1;
}
}


//start set button image ,position,scale,intractive and mode.
//var secPageFullScrntextureButton2 = new PIXI.Sprite(PIXI.Texture.fromImage("public_html/images/Buttons/FullScreenButton.png", true, PIXI.SCALE_MODES.LINEAR)); 

var secPageFullScrntextureButton2 = resources["public_html/images/Buttons/FullScreenButton.png"].texture;
   var secPageFullScrntextureButtonDown2 = resources["public_html/images/Buttons/FullScreenButtonDown.png"].texture;
   var secPageFullScrntextureButtonOver2 = resources["public_html/images/Buttons/FullScreenButtonHover.png"].texture;

    var secPageFullScrnButton = new PIXI.Sprite(secPageFullScrntextureButton2);
      secPageFullScrnButton=setPosition(secPageFullScrnButton,-150,240);
 secPageFullScrnButton.scale.set(0.2);

//    button2=setPosition(button2,app.renderer.width/2,app.renderer.height/1.2);
    secPageFullScrnButton=reSetAnchor(secPageFullScrnButton);
    secPageFullScrnButton.interactive = true;
    secPageFullScrnButton.buttonMode = true;

    secondBgScreen.addChild(secPageFullScrnButton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    secPageFullScrnButton.on('pointerdown', secPageFullScrnonButtonDown2);
    secPageFullScrnButton.on('pointerup', secPageFullScrnonButtonUp2);
    secPageFullScrnButton.on('pointerover', secPageFullScrnonButtonOver2);
    secPageFullScrnButton.on('pointerout', secPageFullScrnonButtonOut2);
    secPageFullScrnButton.on('mouseover', secPageFullScrnonButtonOver2);

   function secPageFullScrnonButtonDown2() {
    this.isdown = true;
      this.texture = secPageFullScrntextureButtonDown2;
    this.alpha = 1;
}
     function secPageFullScrnonButtonUp2() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = secPageFullScrntextureButtonOver2;
    }
    else {
        this.texture = secPageFullScrntextureButton2;
    }
}

function secPageFullScrnonButtonOver2() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = secPageFullScrntextureButtonOver2;
}

function secPageFullScrnonButtonOut2() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = secPageFullScrntextureButton2;
}
  gameSound.play();

}
//End start Button section
var random=0;
var targetSet=0;
function thirdPage(){
    //sky start
  var thirdPageskyImg = new Sprite(resources["public_html/images/sky.png"].texture);

 thirdPageskyImg=reSetAnchor(thirdPageskyImg);
 thirdPageskyImg=setPosition(thirdPageskyImg,app.renderer.width/2,app.renderer.height/8);
 thirdPageskyImg.scale.set(1);
 app.stage.addChild(thirdPageskyImg);
  //sky end
  var cloud1 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud1=reSetAnchor(cloud1);
 cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
 cloud1.scale.set(0.1);
 app.stage.addChild(cloud1);

  var cloud2 = new Sprite(resources["public_html/images/cloud 2.png"].texture);
  cloud2=reSetAnchor(cloud2);
 cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
 cloud2.scale.set(0.1);
 app.stage.addChild(cloud2);

 var cloud3 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud3=reSetAnchor(cloud3);
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 cloud3.scale.set(0.1);
 app.stage.addChild(cloud3);
 
   app.ticker.add(delta => gameLoopCloud(delta));

function gameLoopCloud(delta){
//console.log(cloud3.position.x);

cloud1.x += 1;
 cloud2.x+=1;
  cloud3.x+=1;

if(cloud1.position.x==456){
cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
}
if(cloud2.position.x==480){
  cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
}
if(cloud3.position.x==480){
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 }
}
 
 //Start BG screen
  var thirdPageBgScreen = new Sprite(resources["public_html/images/stadium_withoud_croud.png"].texture);

 thirdPageBgScreen=reSetAnchor(thirdPageBgScreen);
 thirdPageBgScreen=setPosition(thirdPageBgScreen,app.renderer.width/2,app.renderer.height/1.9);
 thirdPageBgScreen.scale.set(1);
 app.stage.addChild(thirdPageBgScreen);
 //End BG Screen

//light Animation
 animatedlight = new PIXI.AnimatedSprite(lightAnimation.animations["light"]);

     animatedlight=setPosition(animatedlight,2,-200);
    animatedlight.scale.set(0.5);//0.32
    animatedlight.animationSpeed = 0.1;
    animatedlight.play();
    animatedlight.visible=true;
  animatedlight.loop=true;
     thirdPageBgScreen.addChild(animatedlight);
 //End light Animation
 //Start Third Page center Panel(set image ,anchor,position,scale)
  var board = new Sprite(resources["public_html/images/board-with-shadow.png"].texture);
  board=reSetAnchor(board);
   board=setPosition(board,2,2);

//  board=setPosition(board,app.renderer.width/2,app.renderer.height/2.5);
  board.scale.set(1);
  thirdPageBgScreen.addChild(board);
  //End Third Page center Panel.

  //Start Text Inside Panel
    targetSet=[10,12,14,16,18,20,22,24,26,28,30];
    random=Math.floor(Math.random()*11);
    let paneltext1 = new Text("Target\n \nof 6 balls" ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAlpha: 4,
    dropShadowAngle: 0.1,
    dropShadowBlur: "",
    dropShadowColor: "#0d0d0d",
    fill: "#f2eeee",
    fontFamily: "Source Serif Variable",
    fontSize: 40,
    fontWeight: "bold",
});
     paneltext1=setPosition(paneltext1,2,-40);

  paneltext1=reSetAnchor(paneltext1);
   paneltext1.scale.set(1);
 board.addChild(paneltext1);

 let paneltext2 = new Text(targetSet[random] ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAlpha: 4,
    dropShadowAngle: -0.4,
    dropShadowBlur: "",
    dropShadowColor: "#e9f99a",
	fill: [
		"#e8b173",
		"#ea3f06"
	],
    fontFamily: "Source Serif Variable",
    fontSize: 60,
    fontWeight: "bold",
});
     paneltext2=setPosition(paneltext2,2,-35);

  paneltext2=reSetAnchor(paneltext2);
   paneltext2.scale.set(1);
 board.addChild(paneltext2);
  //End Text Inside Panel

    //start set button image ,position,scale,intractive and mode.
   var textureButton3 = resources['public_html/images/Buttons/StartButton.png'].texture;
   var textureButtonDown3 = resources['public_html/images/Buttons/StartButtonDown.png'].texture;
   var textureButtonOver3 = resources['public_html/images/Buttons/StartButtonHover.png'].texture;

    var button3 = new PIXI.Sprite(textureButton3);

    button3=setPosition(button3,2,50);
//  button3.scale.set(0.2);

    button3=reSetAnchor(button3);
    button3.interactive = true;
    button3.buttonMode = true;
    board.addChild(button3);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    button3.on('pointerdown', onButtonDown3);
    button3.on('pointerup', onButtonUp3);
        button3.on('pointerover', onButtonOver3);
        button3.on('pointerout', onButtonOut3);
         button3.on('mouseover', onButtonOver3);

   function onButtonDown3() {
    this.isdown = true;
      this.texture = textureButtonDown3;
    this.alpha = 1;
           if(soundEffects== true)

           buttonaudio.play();

      app.stage.addChild(fourthPage());

     }
     function onButtonUp3() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = textureButtonOver3;
    }
    else {
        this.texture = textureButton3;
    }
}

function onButtonOver3() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = textureButtonOver3;
}

function onButtonOut3() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = textureButton3;
}
    //End Button Events

  app.ticker.add(delta => gameLoop(delta));

 var targetPanelScale=0;
 var speedOfTargetPanel=0.02;
//   app.ticker.add(delta => gameLoop(delta));

 function showTargetPanel(delta){
  targetPanelScale+= (speedOfTargetPanel*delta);
if(targetPanelScale >= 0.4){
  targetPanelScale=0.4;
}
popupPanel.scale.set(targetPanelScale);

 }
function gameLoop(delta){
playButtonAnimate(delta);
showTargetPanel(delta);
}

//start Button section
var playButtonScale=0.2;
 var minimumScale=0.18;
 var maximumScale=0.22;
  var speedOfPlayButtonScale=0.001;
 var scaleSwitch=1;



function playButtonAnimate(delta){
 playButtonScale+= scaleSwitch*(speedOfPlayButtonScale*delta);
button3.scale.set(playButtonScale);

if(playButtonScale > maximumScale){
 scaleSwitch= -1;

}
else if(playButtonScale < minimumScale){
    scaleSwitch=1;
}
}
var thirdPagestump1Bottom = new Sprite(resources["public_html/images/stumps.png"].texture);
 thirdPagestump1Bottom=reSetAnchor(thirdPagestump1Bottom);
 thirdPagestump1Bottom=setPosition(thirdPagestump1Bottom,2,235);
 thirdPagestump1Bottom.scale.set(0.6);
  thirdPageBgScreen.addChild(thirdPagestump1Bottom);

  //End Stump section

   //volume and music buttons start
  var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOnButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOnButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOnButtonHover.png"].texture;

    var thirdsceneVolbutton = new PIXI.Sprite(thirdsceneVoltextureButton);
      thirdsceneVolbutton=setPosition(thirdsceneVolbutton,150,250);
 thirdsceneVolbutton.scale.set(0.2);

    thirdsceneVolbutton=reSetAnchor(thirdsceneVolbutton);
    thirdsceneVolbutton.interactive = true;
    thirdsceneVolbutton.buttonMode = true;
    thirdPageBgScreen.addChild(thirdsceneVolbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneVolbutton.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneVolbutton.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneVolbutton.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneVolbutton.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneVolbutton.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
         thirdPageBgScreen.addChild(SoundOffBtn());

}
     function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
        this.texture = thirdsceneVoltextureButton;
    }
}

function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
}

function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButton;
}

var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOnButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOnButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOnButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
      thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,90,250);
 thirdsceneMusicbutton.scale.set(0.2);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    thirdPageBgScreen.addChild(thirdsceneMusicbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
   thirdPageBgScreen.addChild(MusicOffBtn());
}
     function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
        this.texture = thirdsceneMusictextureButton;
    }
}

function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
}

function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
}

//*************Volume and Music OFF Buttons start
function SoundOffBtn(){
    soundEffects=false;
var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOffButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOffButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOffButtonHover.png"].texture;

    var thirdsceneSoundOffBtn = new PIXI.Sprite(thirdsceneVoltextureButton);
      thirdsceneSoundOffBtn=setPosition(thirdsceneSoundOffBtn,150,250);
 thirdsceneSoundOffBtn.scale.set(0.2);

    thirdsceneSoundOffBtn=reSetAnchor(thirdsceneSoundOffBtn);
    thirdsceneSoundOffBtn.interactive = true;
    thirdsceneSoundOffBtn.buttonMode = true;
    thirdPageBgScreen.addChild(thirdsceneSoundOffBtn);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneSoundOffBtn.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneSoundOffBtn.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneSoundOffBtn.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneSoundOffBtn.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneSoundOffBtn.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
    thirdPageBgScreen.removeChild(thirdsceneSoundOffBtn);
    soundEffects=true;
}
     function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
        this.texture = thirdsceneVoltextureButton;
    }
}

function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
}

function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButton;
}
}
function MusicOffBtn(){
var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOffButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOffButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOffButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
      thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,90,250);
 thirdsceneMusicbutton.scale.set(0.2);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    thirdPageBgScreen.addChild(thirdsceneMusicbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
        thirdPageBgScreen.removeChild(thirdsceneMusicbutton);
      gameSound.play();

}
     function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
        this.texture = thirdsceneMusictextureButton;
    }
}

function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
}

function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
}
      gameSound.pause();

}
}

       var batHitAudio = new Audio('public_html/sounds/BatHit.mp3');
     var ballThrowAudio = new Audio('public_html/sounds/Ballthrow.mp3');

 function fourthPage(){
     matchScreen=undefined;
     document.querySelector("#game-canvas").innerHTML = "";
      app = new PIXI.Application({
    width: 400,         // default: 800
    height: 600,        // default: 600
    antialias: true,    // default: false
    transparent: false, // default: false
    resolution: 1,       // default: 1
//    resolution: window.devicePixelRatio || 1
  }
);

app.renderer.autoResize = true;
app.renderer.backgroundColor = 0x061639;
 PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.LINEAR;


var canvas= document.querySelector("#game-canvas").appendChild(app.view);
     clickCount=0;
 //black background start
   var blackbg = new Sprite(resources["public_html/images/blackbg.png"].texture);
   blackbg=reSetAnchor(blackbg);
   blackbg=setPosition(blackbg,app.renderer.width/2,app.renderer.height/2.1);
   blackbg.scale.set(1);
   app.stage.addChild(blackbg);
 //end black

//sky start
   var skyImg = new Sprite(resources["public_html/images/sky.png"].texture);
   skyImg=reSetAnchor(skyImg);
   skyImg=setPosition(skyImg,app.renderer.width/2,app.renderer.height/8);
   skyImg.scale.set(1);
   app.stage.addChild(skyImg);
//sky end

//cloud section start
   var cloud1 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
   cloud1=reSetAnchor(cloud1);
   cloud1=setPosition(cloud1,-250,-20);
   cloud1.scale.set(0.1);
   skyImg.addChild(cloud1);

   var cloud2 = new Sprite(resources["public_html/images/cloud 2.png"].texture);
   cloud2=reSetAnchor(cloud2);
   cloud2=setPosition(cloud2,-400,20);
   cloud2.scale.set(0.1);
   skyImg.addChild(cloud2);

  var cloud3 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud3=reSetAnchor(cloud3);
  cloud3=setPosition(cloud3,-600,-40);
  cloud3.scale.set(0.1);
  skyImg.addChild(cloud3);
  
    app.ticker.add(delta => gameLoopCloud(delta));
function gameLoopCloud(delta){
 console.log(cloud3.position.x);

cloud1.x += 1;
 cloud2.x+=1;
  cloud3.x+=1;

if(cloud1.position.x==257){
   cloud1=setPosition(cloud1,-250,-20);
}
if(cloud2.position.x==255){

   cloud2=setPosition(cloud2,-400,20);
}
if(cloud3.position.x==255){

  cloud3=setPosition(cloud3,-600,-40);

}

}
//cloud section End

//Score board text section
    var scoreboardWithout_no = new Sprite(resources["public_html/images/boxforscorewithtext.png"].texture);
    scoreboardWithout_no=reSetAnchor(scoreboardWithout_no);
    scoreboardWithout_no=setPosition(scoreboardWithout_no,2,-40);
    scoreboardWithout_no.scale.set(1);
    skyImg.addChild(scoreboardWithout_no);


  //End Score Board text section

 //Start Main screen background image
   matchScreen = new Sprite(resources["public_html/images/stadium_withoud_croud.png"].texture);
  matchScreen=reSetAnchor(matchScreen);
  matchScreen=setPosition(matchScreen,app.renderer.width/2,app.renderer.height/1.9);
  matchScreen.scale.set(1);
  app.stage.addChild(matchScreen);
 //end main screen background image

//light Animation
 animatedlight = new PIXI.AnimatedSprite(lightAnimation.animations["light"]);

     animatedlight=setPosition(animatedlight,2,-200);
    animatedlight.scale.set(0.5);//0.32
    animatedlight.animationSpeed = 0.1;
    animatedlight.play();
    animatedlight.visible=true;
  animatedlight.loop=true;
  matchScreen.addChild(animatedlight);
 //End light Animation


   //croud animation start 
    animatedcroudIdle = new PIXI.AnimatedSprite(croudAnimation.animations["croud"]);
     animatedcroudIdle=setPosition(animatedcroudIdle,2,-100);
    animatedcroudIdle.scale.set(0.5);//0.32
    animatedcroudIdle.animationSpeed = 0.1;
    animatedcroudIdle.play();
        animatedcroudIdle.gotoAndStop(0);//animate from frame number

    animatedcroudIdle.visible=true;
    animatedcroudIdle.loop=true;
   matchScreen.addChild(animatedcroudIdle);
   
    animatedcroud = new PIXI.AnimatedSprite(croudAnimation.animations["croud"]);
     animatedcroud=setPosition(animatedcroud,2,-100);
    animatedcroud.scale.set(0.5);//0.32
    animatedcroud.animationSpeed = 0.3;
    animatedcroud.play();
    animatedcroud.visible=false;
    animatedcroud.loop=true;
   matchScreen.addChild(animatedcroud);
   //End croud animation


//instruction arrow with text
var clicktostart = new Sprite(resources["public_html/images/clicktostart.png"].texture);
 clicktostart=reSetAnchor(clicktostart);

 clicktostart=setPosition(clicktostart,-100,50);
 clicktostart.scale.set(0.5);
  matchScreen.addChild(clicktostart);
//Start batman Idle and swing animation
console.log(ballHitAnimation.animations);

console.log(batManIdleAnimation.animations);
animatedBallHit = new PIXI.AnimatedSprite(ballHitAnimation.animations["Energy 06"]);
 animatedBallHit=setPosition(animatedBallHit,-105,50);
 animatedBallHit=reSetAnchor(animatedBallHit);
animatedBallHit.scale.set(1);
animatedBallHit.animationSpeed = 0.5;
  animatedBallHit.loop=false;
animatedBallHit.play();
animatedBallHit.visible=false;
 matchScreen.addChild(animatedBallHit);
 
    animatedIdlebatMan = new PIXI.AnimatedSprite(batManIdleAnimation.animations["batmanIdleframe"]);

     animatedIdlebatMan=setPosition(animatedIdlebatMan,-60,158);
    animatedIdlebatMan.scale.set(0.32);//0.32
    animatedIdlebatMan.animationSpeed = 0.1;
    animatedIdlebatMan.play();
     matchScreen.addChild(animatedIdlebatMan);
console.log(batManSwingAnimation.animations);
    animatedSwingbatMan = new PIXI.AnimatedSprite(batManSwingAnimation.animations["batmanSwingframe"]);
     animatedSwingbatMan=setPosition(animatedSwingbatMan,-138,75);
    animatedSwingbatMan.scale.set(0.32);
    animatedSwingbatMan.loop=false;
    animatedSwingbatMan.animationSpeed = 0.2;
    animatedSwingbatMan.gotoAndPlay(0);//animate from frame number
    animatedSwingbatMan.visible = false;
    animatedSwingbatMan.play();
    matchScreen.addChild(animatedSwingbatMan);
  animatedSwingbatMan.visible=true;
     animatedSwingbatMan.visible=false;

//Start screen Onclick Event
  matchScreen.interactive = true;
var totalRuns = [];
var totalBall=6;
 var ballLeft=6;
 var runsLeft=targetSet[random];
 var targetVx=[-10,-8,-6,-4,0,2,4,6,8,10,12];
var randomVx=0;
var wicket=1;
var wicketcount=0;
   matchScreen.on('pointerdown', (click) => {
       clickCount++;
       console.log("Click Count "+clickCount);
       // clickCount++;
    if(clickCount==2){

      if(currentBallStatus==true && battingStatus==false)  {

                animatedIdlebatMan.visible = false;
                animatedSwingbatMan.gotoAndPlay(0);//animate from frame number
                animatedSwingbatMan.visible = true;
                 battingStatus=true;

//                 console.log(ball.position.y);
                  if(ball.position.y < 170 || ball.position.y > 250){
                      if(soundEffects== true)
                     batHitAudio.play();
                      runs=0;
//                        console.log(runs);
                        currentBallCounter=0;
//                     BatHitBall=true;
                   animatedBallHit.visible=false;
                    ballOntheWay=true;
                     }
                  if(ball.position.y > 170 && ball.position.y < 205){//215
                    if(soundEffects== true)
                     batHitAudio.play();

                  randomVx=Math.floor(Math.random()*11);

                      runs=2;
//                        console.log(runs);
                        currentBallCounter=0;
                     //BatHitBall=true;
                     ballOntheWay=false;
                     setTimeout(function(){
                       BatHitBall=true;
                       animatedBallHit=setPosition(animatedBallHit,ball.position.x,ball.position.y);
                       animatedBallHit.visible=true;
                       animatedBallHit.gotoAndPlay(0);

},50);
                     }
                 if(ball.position.y > 205 && ball.position.y < 220){
                     if(soundEffects== true)
                     batHitAudio.play();
                 randomVx=Math.floor(Math.random()*11);

                        runs=4;
//                    console.log(runs);
                     currentBallCounter=0;
                    // BatHitBall=true;
                     ballOntheWay=false;
                     setTimeout(function(){
                       BatHitBall=true;
                       animatedBallHit=setPosition(animatedBallHit,ball.position.x,ball.position.y);
                       animatedBallHit.visible=true;
                       animatedBallHit.gotoAndPlay(0);

                    },50);
                     }
                     if(ball.position.y > 220 && ball.position.y < 250){
                         if(soundEffects== true)
                     batHitAudio.play();
                  randomVx=Math.floor(Math.random()*11);

                         runs=6;
//                        console.log(runs);
                     currentBallCounter=0;
                     BatHitBall=true;
                     ballOntheWay=false;
                    // setTimeout(function(){
                       //BatHitBall=true;
                       animatedBallHit=setPosition(animatedBallHit,ball.position.x,ball.position.y);
                       animatedBallHit.visible=true;
                       animatedBallHit.gotoAndPlay(0);
                    
                  //  },10);
                     }


    totalRuns.push(runs);//add balls and run in array.
//    console.log(totalRuns);
    ballLeft=totalBall-totalRuns.length; //check total ball left.
//    console.log("Ball Left "+ballLeft);
    RemainingballLeftText.text=ballLeft;
    var countRunsToLeft = totalRuns.reduce(function(a, b){  //addition of runs for left runs.
        return a + b;
    }, 0);
    runsLeft=targetSet[random]-countRunsToLeft;  //check runs left.
//    console.log("Runs Left "+runsLeft);
    RemainingrunsLeftText.text=runsLeft;


 }
  }
    else if(clickCount==1){
      clicktostart.visible=false;   //Visible false at first click game play

        if(currentBallStatus==false){

            ballThrowStatus=true;

        currentBallCounter=0;
        animatedIdlebowler.visible = false;
        animatedSwingbowler.gotoAndPlay(0);//animate from frame number
        animatedSwingbowler.visible = true;
        progressStatus=false;
        currentBallStatus=true;


    }
    }
    });

   var ballThrowStatus=false;
       var progressStatus=true;
       var currentBallStatus=false;
       var currentBallCounter=0;
       var ballDelay=3;
       var battingStatus=false;


         var timeToIdle=1;
         var timeCounter=0;
         var bowlertimeCounter=0;

          var BatHitBall=false;
            //Onclick event end

//Start progressbar loop animation
// var playProgressbarFrontScale=0.2;
 var minimumScale=5;
 var maximumScale=185;
  var speedOfPlayProgressbarFrontScale=5;
 var scaleSwitch=1;
  var progressbarScale=0;

function ProgressbarAnimate(delta){
    if(progressStatus == true){
    progressbarScale+= scaleSwitch*(speedOfPlayProgressbarFrontScale);
    progressbarfront.width=progressbarScale;

    if(progressbarScale >= maximumScale){
     scaleSwitch= -1;
       }
        else if(progressbarScale <= minimumScale){
       scaleSwitch=1;
}
}
}
//end progressbar loop animation.

//start ball throw section.
var ballOntheWay=false;
var ballScale=0.1;//0.01
 var maximumBallScale=0.06;//0.02
  var speedOfBallScale=0.0005;
function throwBall(){
 ball=setPosition(ball,2,-50);
 ball.visible=true;
            if(soundEffects== true)
          ballThrowAudio.play();
ball.scale.set(0.1);//0.01
ballScale=0.1;//0.01
 ballOntheWay=true;
}

function throwBallAnimate(delta){
    //Update the cat's velocity
  ball.vy = 2*(progressbarfront.width/maximumScale)+1;
  ballScale+=(speedOfBallScale*delta);
ball.scale.set(ballScale);
 //position to make it move
  ball.y += ball.vy;
if(ballScale > maximumBallScale){
 ballScale=maximumBallScale;
}
}
//End ball throw section.

//var ballSpecificPosition=setPosition(ballSpecificPosition,2,225);
//var BatHitBall =false;
var ballDirectionSwitch=-1;
var ballSwitchDone=false;
var SixPosition=-100;
var minumumBallScale=0.01;
var speedOfHitBall=0.00008;

function HitBallAnimate(delta){

     //Update the cat's velocity
  ballScale-=(speedOfHitBall*delta);
ball.scale.set(ballScale);
 //position to make it move
 if(ballDirectionSwitch != 0){
  ball.vy = ballDirectionSwitch*(runs);
  ball.y += ball.vy;
  ball.vx=targetVx[randomVx]*0.1;
  ball.x +=ball.vx;
 }
  if(runs == 6){

//      console.log(ball.position.y);
      if(ballSwitchDone==false){
      if(ball.position.y < -200 && ballSwitchDone==false){
      ballDirectionSwitch=1;
      ballSwitchDone=true;
      }
      else{
       ballDirectionSwitch=-1.2;
      }
  }
  else if(ball.position.y > SixPosition){
     ball.visible=false;

     ballDirectionSwitch=0;
     ball.vy=0;
     ballSwitchDone=true;
     six.position=ball.position;
     six.visible=true;
   animatedcroud.visible=true;

  }
  }
else if(runs == 4){
//      console.log(ball.position.y);
      if(ballSwitchDone==false){
      if(ball.position.y < -75 && ballSwitchDone==false){
               ball.visible=false;

      ballDirectionSwitch=0;
     ball.vy=0;
     ballSwitchDone=true;
     four.position=ball.position;
     four.visible=true;
      }
      else{
       ballDirectionSwitch=-1;
      }
      }

      }

else if(runs == 2){
//      console.log(ball.position.y);
      if(ballSwitchDone==false){
      if(ball.position.y < -30 && ballSwitchDone==false){
                         ball.visible=false;

       ballDirectionSwitch=0;
      ball.vy=0;
      ballSwitchDone=true;
      two.position=ball.position;
      two.visible=true;
      }
      else{
       ballDirectionSwitch=-1;
      }
      }
      }

if(ballScale < minumumBallScale){
 ballScale=minumumBallScale;
}
}
//start hit ball animation
var RemainingrunsLeftText;
 RemainingrunsLeftText = new Text (runsLeft ,{ align: "center",
    breakWords: true,
//    dropShadow: true,
//    dropShadowAngle: 0.1,
//    dropShadowBlur: 4,
//    dropShadowColor: "#e9f99a",
	    fill: "white",

    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
    });

   RemainingrunsLeftText=reSetAnchor(RemainingrunsLeftText);
   RemainingrunsLeftText=setPosition(RemainingrunsLeftText,11,15);
   RemainingrunsLeftText.scale.set(0.8);
   scoreboardWithout_no.addChild(RemainingrunsLeftText);

  var RemainingballLeftText;

    RemainingballLeftText = new Text(ballLeft ,{ align: "center",
    breakWords: true,
//    dropShadow: true,
//    dropShadowAngle: 0.1,
//    dropShadowBlur: 4,
//    dropShadowColor: "#e9f99a",
	    fill: "white",
    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
    });

   RemainingballLeftText=reSetAnchor(RemainingballLeftText);
   RemainingballLeftText=setPosition(RemainingballLeftText,150,15);
   RemainingballLeftText.scale.set(0.8);
   scoreboardWithout_no.addChild(RemainingballLeftText);

  var RemainingwicketText;
   RemainingwicketText = new Text(wicketcount ,{ align: "center",
    breakWords: true,
//    dropShadow: true,
//    dropShadowAngle: 0.1,
//    dropShadowBlur: 4,
//    dropShadowColor: "#e9f99a",
	    fill: "white",
    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
    });

   RemainingwicketText=reSetAnchor(RemainingwicketText);
    RemainingwicketText=setPosition(RemainingwicketText,-135,15);
  RemainingwicketText.scale.set(0.8);
  scoreboardWithout_no.addChild(RemainingwicketText);
//End hit ball animation.

//start runs images
six = new Sprite(resources["public_html/images/six_number.png"].texture);
 six=reSetAnchor(six);
 six=setPosition(six,2,-40);
 six.scale.set(0.1);
 six.visible=false;
  matchScreen.addChild(six);

  four = new Sprite(resources["public_html/images/four_number.png"].texture);
 four=reSetAnchor(four);
 four=setPosition(four,2,-40);
 four.scale.set(0.1);
 four.visible=false;
  matchScreen.addChild(four);

 two = new Sprite(resources["public_html/images/two_number.png"].texture);
 two=reSetAnchor(two);
 two=setPosition(two,2,-40);
 two.scale.set(0.1);
 two.visible=false;
  matchScreen.addChild(two);
//runs images end

//start bowler Idle and Swing spritesheet section.
 ball = new Sprite(resources["public_html/images/ball.png"].texture);
 ball=reSetAnchor(ball);
 ball=setPosition(ball,2,-40);
 ball.scale.set(0.02);
 ball.visible=false;
  matchScreen.addChild(ball);


    animatedIdlebowler = new PIXI.AnimatedSprite(bowlerIdleAnimation.animations["bowlerIdle"]);
     animatedIdlebowler=setPosition(animatedIdlebowler,-50,-80);
    animatedIdlebowler.scale.set(0.14);
    animatedIdlebowler.animationSpeed = 0.12;
    animatedIdlebowler.play();
    matchScreen.addChild(animatedIdlebowler);

    animatedSwingbowler = new PIXI.AnimatedSprite(bowlerSwingAnimation.animations["bowlerSwingFrame"]);
     animatedSwingbowler=setPosition(animatedSwingbowler,-20,-40);
    animatedSwingbowler.scale.set(0.14);
    animatedSwingbowler.animationSpeed = 0.1;
    animatedSwingbowler.visible=false;
    animatedSwingbowler.play();
    animatedSwingbowler.loop=false;
    matchScreen.addChild(animatedSwingbowler);
  //End bowler spritesheet section

   //start Stump section (gif , position , anchor,scale)
   var fourthPagestump1Top = new Sprite(resources["public_html/images/stumps.png"].texture);
 fourthPagestump1Top=reSetAnchor(fourthPagestump1Top);
 fourthPagestump1Top=setPosition(fourthPagestump1Top,2,-25);
 fourthPagestump1Top.scale.set(0.3);
  matchScreen.addChild(fourthPagestump1Top);

  stumpBottom = new Sprite(resources["public_html/images/stumps.png"].texture);
 stumpBottom=reSetAnchor(stumpBottom);
 stumpBottom=setPosition(stumpBottom,2,235);
 stumpBottom.scale.set(0.6);
 stumpBottom.visible=true;
  matchScreen.addChild(stumpBottom);

  //stump animation start
  stumpSpritesheet = PIXI.loader.resources["public_html/images/stump-animation.json"].spritesheet;
    animatedStump = new PIXI.AnimatedSprite(stumpSpritesheet.animations["stumpsBreaking"]);
     animatedStump=setPosition(animatedStump,2,235);
  animatedStump.scale.set(0.6);
   animatedStump.loop=false;
    animatedStump.animationSpeed = 0.040;
    animatedStump.visible=false;
//    animatedStump.gotoAndPlay(0);
    animatedStump.play();
   matchScreen.addChild(animatedStump);
  //stump animation end.
  //End stump section.

//volume ON buttons start
   var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOnButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOnButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOnButtonHover.png"].texture;

    var thirdsceneVolbutton = new PIXI.Sprite(thirdsceneVoltextureButton);
    thirdsceneVolbutton=setPosition(thirdsceneVolbutton,180,265);
    thirdsceneVolbutton.scale.set(0.1);

    thirdsceneVolbutton=reSetAnchor(thirdsceneVolbutton);
    thirdsceneVolbutton.interactive = true;
    thirdsceneVolbutton.buttonMode = true;
    matchScreen.addChild(thirdsceneVolbutton);

    //Start Button Events
    thirdsceneVolbutton.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneVolbutton.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneVolbutton.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneVolbutton.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneVolbutton.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
    this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
    matchScreen.addChild(SoundOffBtn());
     }

    function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
    this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
    this.texture = thirdsceneVoltextureButton;
    }
    }

    function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
    return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
    }

   function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
    return;
    }
    this.texture = thirdsceneVoltextureButton;
    }
//End Volume ON button

//Start Music ON button
   var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOnButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOnButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOnButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
    thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,150,265);
    thirdsceneMusicbutton.scale.set(0.1);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    matchScreen.addChild(thirdsceneMusicbutton);

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
    this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
    matchScreen.addChild(MusicOffBtn());

    }

    function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
    this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
    this.texture = thirdsceneMusictextureButton;
    }
    }

    function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
         return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
    }

   function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
    }

 //*************Volume and Music OFF Buttons start
function SoundOffBtn(){
var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOffButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOffButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOffButtonHover.png"].texture;

    var thirdsceneSoundOffBtn = new PIXI.Sprite(thirdsceneVoltextureButton);
      thirdsceneSoundOffBtn=setPosition(thirdsceneSoundOffBtn,180,265);
 thirdsceneSoundOffBtn.scale.set(0.1);

    thirdsceneSoundOffBtn=reSetAnchor(thirdsceneSoundOffBtn);
    thirdsceneSoundOffBtn.interactive = true;
    thirdsceneSoundOffBtn.buttonMode = true;
    matchScreen.addChild(thirdsceneSoundOffBtn);

    //Start Button Events
    thirdsceneSoundOffBtn.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneSoundOffBtn.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneSoundOffBtn.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneSoundOffBtn.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneSoundOffBtn.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
    matchScreen.removeChild(thirdsceneSoundOffBtn);

}
     function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
        this.texture = thirdsceneVoltextureButton;
    }
}

function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
}

function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButton;
}
}
function MusicOffBtn(){
var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOffButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOffButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOffButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
      thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,150,265);
 thirdsceneMusicbutton.scale.set(0.1);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    matchScreen.addChild(thirdsceneMusicbutton);

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
        matchScreen.removeChild(thirdsceneMusicbutton);
       gameSound.play();

}
     function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
        this.texture = thirdsceneMusictextureButton;
    }
}

function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
}

function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
}
  gameSound.pause();

}
 //End sound OFF and Music OFF buttons

 //Start progressbar section
 var progressbarBg = new Sprite(resources["public_html/images/progressbarbg.png"].texture);
 progressbarBg=reSetAnchor(progressbarBg);
  progressbarBg.rotation=Math.PI/2;

 progressbarBg=setPosition(progressbarBg,-170,150);
 progressbarBg.scale.set(0.2);
  matchScreen.addChild(progressbarBg);
  
  var powerguagetxt = new Sprite(resources["public_html/images/powergaugeText.png"].texture);
 powerguagetxt=reSetAnchor(powerguagetxt);
  powerguagetxt.rotation=Math.PI/2;

 powerguagetxt=setPosition(powerguagetxt,-170,150);
 powerguagetxt.scale.set(0.3);
  matchScreen.addChild(powerguagetxt);
  
  
  var progressbarfront = new Sprite(resources["public_html/images/progressbarfront.png"].texture);
  progressbarfront.rotation=Math.PI/2;
 progressbarfront=setPosition(progressbarfront,-170,242);
 progressbarfront.anchor.x = 1;
progressbarfront.anchor.y = 0.5;

 progressbarfront.scale.set(0.2);
 progressbarfront.width = 0;
  matchScreen.addChild(progressbarfront);
 //End progressbar section.


     app.ticker.remove(gameLoopGame);
   //  app.ticker.stop(gameLoopGame);
  //Start gameloop
    // if(gameLoopStarted==false){
         gameLoopStarted=true;
   app.ticker.add(delta => gameLoopGame(delta));
   //      }
     var gameLoopRunning=true;
  function gameLoopGame(delta){
      if(gameLoopRunning==false)
          return;


     // console.log("GameLoop Game")
    ProgressbarAnimate(delta);
if(animatedSwingbatMan.visible==true){
     timeCounter+=(0.01);
if(timeCounter > timeToIdle){
    animatedIdlebatMan.gotoAndPlay(0);
    animatedIdlebatMan.visible=true;
    animatedSwingbatMan.visible=false;
    timeCounter=0;
}
}

if(animatedSwingbowler.visible==true){
     bowlertimeCounter+=(0.01);
     if(ballThrowStatus==true){
     if(bowlertimeCounter >0.5){
       throwBall();
       ballThrowStatus=false;

     }
 }
if(bowlertimeCounter > timeToIdle){
    animatedIdlebowler.gotoAndPlay(0);
    animatedIdlebowler.visible=true;
    animatedSwingbowler.visible=false;
    bowlertimeCounter=0;
}
}

if(currentBallStatus==true){

    if(ball.position.y > 255 && ball.position.y < 260 && stumpBottom.visible==true){
    wicketcount=wicket++;
      stumpBottom.visible=false;
       animatedStump.visible=true;
//                     currentBallCounter=0;
//                     BatHitBall=true;
//                     ballOntheWay=false;
       RemainingwicketText.text=wicketcount;

                     }
  currentBallCounter+=0.01;
  if(currentBallCounter >=ballDelay){


      currentBallCounter=0;
    currentBallStatus=false;
    progressStatus=true;
    clickCount=0;
    battingStatus=false;
    ballOntheWay=false;
    ballThrowStatus=false;
    BatHitBall=false;
    ball.visible=false;
    ballSwitchDone=false;
    ballDirectionSwitch=-1;
    speedOfBallScale=0.0005;
     four.visible=false;
     six.visible=false;
     two.visible=false;
stumpBottom.visible=true;
 animatedStump.visible=false;
animatedcroud.visible=false;
  setTimeout(function(){

 if(runsLeft <= 0){
     app.ticker.remove(gameLoopGame);
     gameLoopRunning=false;
    // app.ticker.stop();
    app.stage.addChild(victoryScreen());
     }

    if(totalRuns.length==6){ //check if array/ball length is 6
        var sum = totalRuns.reduce(function(a, b){  //addition of runs.
        return a + b;
    }, 0);
    console.log('Your Total runs'+sum);
    console.log('Required runs'+targetSet[random]);

    if(sum >= targetSet[random]){  //check if runs is greater than equal to target run.
        console.log('You win');
         app.ticker.remove(gameLoopGame);
        gameLoopRunning=false;
      //  app.ticker.stop();
        app.stage.addChild(victoryScreen());
      totalRuns = [];
    }
    else{
        console.log('You lose.');
        app.ticker.remove(gameLoopGame);
        gameLoopRunning=false;

       // app.ticker.stop();
        app.stage.addChild(lostScreen());
         totalRuns = [];
    }
    }
     }, 100);

  }

}


if(ballOntheWay==true){
 throwBallAnimate(delta);
}


if(BatHitBall==true){
HitBallAnimate(delta);
}
}
//Game loop function end.

 }


 function lostScreen(){
     //sky start
  var skyImg = new Sprite(resources["public_html/images/sky.png"].texture);

 skyImg=reSetAnchor(skyImg);
 skyImg=setPosition(skyImg,app.renderer.width/2,app.renderer.height/8);
 skyImg.scale.set(1);
 app.stage.addChild(skyImg);
  //sky end
  var cloud1 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud1=reSetAnchor(cloud1);
 cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
 cloud1.scale.set(0.1);
 app.stage.addChild(cloud1);

  var cloud2 = new Sprite(resources["public_html/images/cloud 2.png"].texture);
  cloud2=reSetAnchor(cloud2);
 cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
 cloud2.scale.set(0.1);
 app.stage.addChild(cloud2);

 var cloud3 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud3=reSetAnchor(cloud3);
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 cloud3.scale.set(0.1);
 app.stage.addChild(cloud3);
 
    app.ticker.add(delta => gameLoopCloud(delta));
function gameLoopCloud(delta){
// console.log(cloud3.position.x);
cloud1.x += 1;
 cloud2.x+=1;
  cloud3.x+=1;

if(cloud1.position.x==456){
cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
}
if(cloud2.position.x==480){
  cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
}
if(cloud3.position.x==480){
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 }
}

 //ground with stadium image
  var lostScreen = new Sprite(resources["public_html/images/stadium_withoud_croud.png"].texture);

 lostScreen=reSetAnchor(lostScreen);
 lostScreen=setPosition(lostScreen,app.renderer.width/2,app.renderer.height/1.9);
 lostScreen.scale.set(1);
 app.stage.addChild(lostScreen);
//start lost screen center Panel(set image ,anchor,position,scale)

//light Animation
 animatedlight = new PIXI.AnimatedSprite(lightAnimation.animations["light"]);

     animatedlight=setPosition(animatedlight,2,-200);
    animatedlight.scale.set(0.5);//0.32
    animatedlight.animationSpeed = 0.1;
    animatedlight.play();
    animatedlight.visible=true;
  animatedlight.loop=true;
  lostScreen.addChild(animatedlight);
 //End light Animation

  var LostScreenPanel = new Sprite(resources["public_html/images/board-with-shadow.png"].texture);
    LostScreenPanel=reSetAnchor(LostScreenPanel);
//  LostScreenPanel=setPosition(LostScreenPanel,app.renderer.width/2,app.renderer.height/2.3);
  LostScreenPanel.scale.set(1);
  lostScreen.addChild(LostScreenPanel);
  //End lost screen center Panel.

 //start lost screen panel text
 let popuptext1 = new Text('A CLOSE FINISH\nBUT NOT A VICTORY' ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAngle: 0.1,
    dropShadowBlur: 4,
    dropShadowColor: "#320b0b",
	fill: [
		"#f3f1f1",
		"white"
	],
    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
});

   popuptext1=reSetAnchor(popuptext1);
 popuptext1=setPosition(popuptext1,2,-80);
  popuptext1.scale.set(0.7);
  LostScreenPanel.addChild(popuptext1);

  let tryAgainText = new Text('TRY AGAIN' ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAngle: 0.1,
    dropShadowBlur: 4,
    dropShadowColor: "#320b0b",
	fill: [
		"#f7f6f2",
		"#e8ea7b"
	],
    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
});

   tryAgainText=reSetAnchor(tryAgainText);
 tryAgainText=setPosition(tryAgainText,2,2);
  tryAgainText.scale.set(1);
  LostScreenPanel.addChild(tryAgainText);
   //End lost screen panel text

//Start Try Again Button
var refreshtextureButton = resources["public_html/images/Buttons/RefreshButton.png"].texture;
   var refreshtextureButtonDown = resources["public_html/images/Buttons/RefreshButtonDown.png"].texture;
   var refreshtextureButtonOver = resources["public_html/images/Buttons/RefreshButtonHover.png"].texture;

    var refreshbutton = new PIXI.Sprite(refreshtextureButton);
        refreshbutton=setPosition(refreshbutton,2,50);

 refreshbutton.scale.set(0.2);

    refreshbutton=reSetAnchor(refreshbutton);
    refreshbutton.interactive = true;
    refreshbutton.buttonMode = true;
    LostScreenPanel.addChild(refreshbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    refreshbutton.on('pointerdown', refreshonButtonDown);
    refreshbutton.on('pointerup', refreshonButtonUp);
    refreshbutton.on('pointerover', refreshonButtonOver);
    refreshbutton.on('pointerout', refreshonButtonOut);
    refreshbutton.on('mouseover', refreshonButtonOver);

   function refreshonButtonDown() {
    this.isdown = true;
      this.texture = refreshtextureButtonDown;
    this.alpha = 1;
    app.stage.addChild(thirdPage());

}
     function refreshonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = refreshtextureButtonOver;
    }
    else {
        this.texture = refreshtextureButton;
    }
}

function refreshonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = refreshtextureButtonOver;
}

function refreshonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = refreshtextureButton;
}

     //End Try Again Button

     var lostScreenstump1Bottom = new Sprite(resources["public_html/images/stumps.png"].texture);
 lostScreenstump1Bottom=reSetAnchor(lostScreenstump1Bottom);
 lostScreenstump1Bottom=setPosition(lostScreenstump1Bottom,2,235);
 lostScreenstump1Bottom.scale.set(0.6);
  lostScreen.addChild(lostScreenstump1Bottom);

        //volume and music buttons start
  var lostScrnVoltextureButton = resources["public_html/images/Buttons/SoundOnButton.png"].texture;
   var lostScrnVoltextureButtonDown = resources["public_html/images/Buttons/SoundOnButtonDown.png"].texture;
   var lostScrnVoltextureButtonOver = resources["public_html/images/Buttons/SoundOnButtonHover.png"].texture;

    var lostScrnVolbutton = new PIXI.Sprite(lostScrnVoltextureButton);
      lostScrnVolbutton=setPosition(lostScrnVolbutton,150,250);
 lostScrnVolbutton.scale.set(0.2);

    lostScrnVolbutton=reSetAnchor(lostScrnVolbutton);
    lostScrnVolbutton.interactive = true;
    lostScrnVolbutton.buttonMode = true;
    lostScreen.addChild(lostScrnVolbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    lostScrnVolbutton.on('pointerdown', lostScrnVolonButtonDown);
    lostScrnVolbutton.on('pointerup', lostScrnVolonButtonUp);
    lostScrnVolbutton.on('pointerover', lostScrnVolonButtonOver);
    lostScrnVolbutton.on('pointerout', lostScrnVolonButtonOut);
    lostScrnVolbutton.on('mouseover', lostScrnVolonButtonOver);

   function lostScrnVolonButtonDown() {
    this.isdown = true;
      this.texture = lostScrnVoltextureButtonDown;
    this.alpha = 1;
   lostScreen.addChild(SoundOffBtn());
}
     function lostScrnVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = lostScrnVoltextureButtonOver;
    }
    else {
        this.texture = lostScrnVoltextureButton;
    }
}

function lostScrnVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = lostScrnVoltextureButtonOver;
}

function lostScrnVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = lostScrnVoltextureButton;
}

var lostScrnMusictextureButton = resources["public_html/images/Buttons/MusicOnButton.png"].texture;
   var lostScrnMusictextureButtonDown = resources["public_html/images/Buttons/MusicOnButtonDown.png"].texture;
   var lostScrnMusictextureButtonOver = resources["public_html/images/Buttons/MusicOnButtonHover.png"].texture;

    var lostScrnMusicbutton = new PIXI.Sprite(lostScrnMusictextureButton);
      lostScrnMusicbutton=setPosition(lostScrnMusicbutton,90,250);
 lostScrnMusicbutton.scale.set(0.2);

    lostScrnMusicbutton=reSetAnchor(lostScrnMusicbutton);
    lostScrnMusicbutton.interactive = true;
    lostScrnMusicbutton.buttonMode = true;
    lostScreen.addChild(lostScrnMusicbutton);

    //Start Button Events
    lostScrnMusicbutton.on('pointerdown', lostScrnMusiconButtonDown);
    lostScrnMusicbutton.on('pointerup', lostScrnMusiconButtonUp);
    lostScrnMusicbutton.on('pointerover', lostScrnMusiconButtonOver);
    lostScrnMusicbutton.on('pointerout', lostScrnMusiconButtonOut);
    lostScrnMusicbutton.on('mouseover', lostScrnMusiconButtonOver);

   function lostScrnMusiconButtonDown() {
    this.isdown = true;
      this.texture = lostScrnMusictextureButtonDown;
    this.alpha = 1;
 lostScreen.addChild(MusicOffBtn());
}
     function lostScrnMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = lostScrnMusictextureButtonOver;
    }
    else {
        this.texture = lostScrnMusictextureButton;
    }
}

function lostScrnMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = lostScrnMusictextureButtonOver;
}

function lostScrnMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = lostScrnMusictextureButton;
}

//*************Volume and Music OFF Buttons start
function SoundOffBtn(){
var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOffButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOffButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOffButtonHover.png"].texture;

    var thirdsceneSoundOffBtn = new PIXI.Sprite(thirdsceneVoltextureButton);
      thirdsceneSoundOffBtn=setPosition(thirdsceneSoundOffBtn,150,250);
 thirdsceneSoundOffBtn.scale.set(0.2);

    thirdsceneSoundOffBtn=reSetAnchor(thirdsceneSoundOffBtn);
    thirdsceneSoundOffBtn.interactive = true;
    thirdsceneSoundOffBtn.buttonMode = true;
    lostScreen.addChild(thirdsceneSoundOffBtn);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneSoundOffBtn.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneSoundOffBtn.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneSoundOffBtn.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneSoundOffBtn.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneSoundOffBtn.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
    lostScreen.removeChild(thirdsceneSoundOffBtn);

}
     function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
        this.texture = thirdsceneVoltextureButton;
    }
}

function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
}

function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButton;
}
}
function MusicOffBtn(){
var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOffButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOffButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOffButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
      thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,90,250);
 thirdsceneMusicbutton.scale.set(0.2);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    lostScreen.addChild(thirdsceneMusicbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
        lostScreen.removeChild(thirdsceneMusicbutton);
      gameSound.play();

}
     function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
        this.texture = thirdsceneMusictextureButton;
    }
}

function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
}

function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
}
      gameSound.pause();

}
 }

 function victoryScreen(){
   //sky start
 var skyImg = new Sprite(resources["public_html/images/sky.png"].texture);

 skyImg=reSetAnchor(skyImg);
 skyImg=setPosition(skyImg,app.renderer.width/2,app.renderer.height/8);
 skyImg.scale.set(1);
 app.stage.addChild(skyImg);
  //sky end
   var cloud1 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud1=reSetAnchor(cloud1);
 cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
 cloud1.scale.set(0.1);
 app.stage.addChild(cloud1);

  var cloud2 = new Sprite(resources["public_html/images/cloud 2.png"].texture);
  cloud2=reSetAnchor(cloud2);
 cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
 cloud2.scale.set(0.1);
 app.stage.addChild(cloud2);

 var cloud3 = new Sprite(resources["public_html/images/cloud 1.png"].texture);
  cloud3=reSetAnchor(cloud3);
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 cloud3.scale.set(0.1);
 app.stage.addChild(cloud3);
 
    app.ticker.add(delta => gameLoopCloud(delta));
function gameLoopCloud(delta){
// console.log(cloud3.position.x);

cloud1.x += 1;
 cloud2.x+=1;
  cloud3.x+=1;

if(cloud1.position.x==456){
cloud1=setPosition(cloud1,app.renderer.width/-8,app.renderer.height/13);
}
if(cloud2.position.x==480){
  cloud2=setPosition(cloud2,app.renderer.width/-2,app.renderer.height/7);
}
if(cloud3.position.x==480){
 cloud3=setPosition(cloud3,app.renderer.width/-1.1,app.renderer.height/13);
 }


}

 //ground with stadium image
  var victoryScreen = new Sprite(resources["public_html/images/stadium_withoud_croud.png"].texture);

 victoryScreen=reSetAnchor(victoryScreen);
 victoryScreen=setPosition(victoryScreen,app.renderer.width/2,app.renderer.height/1.9);
 victoryScreen.scale.set(1);
 app.stage.addChild(victoryScreen);

//light Animation
 animatedlight = new PIXI.AnimatedSprite(lightAnimation.animations["light"]);

     animatedlight=setPosition(animatedlight,2,-200);
    animatedlight.scale.set(0.5);//0.32
    animatedlight.animationSpeed = 0.1;
    animatedlight.play();
    animatedlight.visible=true;
  animatedlight.loop=true;
  victoryScreen.addChild(animatedlight);
 //End light Animation

 //start lost screen center Panel(set image ,anchor,position,scale)
  var LostScreenPanel = new Sprite(resources["public_html/images/board-with-shadow.png"].texture);
  LostScreenPanel=reSetAnchor(LostScreenPanel);
  LostScreenPanel=setPosition(LostScreenPanel,(app.renderer.width/2)-5,app.renderer.height/2.5);
  LostScreenPanel.scale.set(1.2);
  app.stage.addChild(LostScreenPanel);
  //End lost screen center Panel.

//blast Animation
   animatedBlast = new PIXI.AnimatedSprite(blastAnimation.animations["blast"]);
     animatedBlast=setPosition(animatedBlast,2,2);
    animatedBlast.scale.set(2);
    animatedBlast.animationSpeed = 0.5;
    animatedBlast.loop=true;
    animatedBlast.play();
    animatedBlast.visible=true;
    app.stage.addChild(animatedBlast);
    //End



  let victoryscreenText1 = new Text('YOU WON !!' ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAngle: 0.1,
    dropShadowBlur: 4,
    dropShadowColor: "#320b0b",
	fill: [
		"#f3f1f1",
		"white"
	],
    fontFamily: "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
});

   victoryscreenText1=reSetAnchor(victoryscreenText1);
 victoryscreenText1=setPosition(victoryscreenText1,app.renderer.width/2,app.renderer.height/4);
  victoryscreenText1.scale.set(1);
  app.stage.addChild(victoryscreenText1);

   let victoryscreenText2 = new Text('CAPTAIN COOL' ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAngle: 0.1,
    dropShadowBlur: 4,
    dropShadowColor: "#320b0b",
	fill: [
		"#f3f1f1",
		"white"
	],
    fontFamily:  "Source Serif Variable",
    fontSize: 30,
    fontWeight: "bold",
    stroke: "#f9f9fa",
});

   victoryscreenText2=reSetAnchor(victoryscreenText2);
 victoryscreenText2=setPosition(victoryscreenText2,app.renderer.width/2,app.renderer.height/3);
  victoryscreenText2.scale.set(0.8);
  app.stage.addChild(victoryscreenText2);


   let victoryscreenText3 = new Text('FINISHES IN STYLE !!' ,{ align: "center",
    breakWords: true,
    dropShadow: true,
    dropShadowAngle: 0.1,
    dropShadowBlur: 4,
    dropShadowColor: "#320b0b",
	fill: [
		"#f3f1f1",
		"white"
	],
    fontFamily:  "Source Serif Variable",
    fontSize: 17,
    fontWeight: "bold",
    stroke: "#f9f9fa",
});

   victoryscreenText3=reSetAnchor(victoryscreenText3);
 victoryscreenText3=setPosition(victoryscreenText3,app.renderer.width/2,app.renderer.height/2.6);
  victoryscreenText3.scale.set(1);
  app.stage.addChild(victoryscreenText3);



 //start panel2 text
 var lostscreen = PIXI.Sprite.fromImage('public_html/images/victory-scrn-white-board.png');
 lostscreen=reSetAnchor(lostscreen);
 lostscreen=setPosition(lostscreen,app.renderer.width/2,app.renderer.height/1.8);
 lostscreen.scale.set(1);
  lostscreen.interactive = true;
    lostscreen.buttonMode = true;
 app.stage.addChild(lostscreen);
     lostscreen.on('pointerup', onClick);

   function onClick(event) {
    this.isdown = true;
    this.alpha = 1;
    return window.open("http://localhost/form/index.php");
     }

  
 //end panel2 text

 //Start play Again Button
 var texturePlayAgainBtn = resources["public_html/images/play again button.png"].texture;
   var texturePlayAgainButtonDown = resources["public_html/images/play again button.png"].texture;
//   var textureButtonOver3 = PIXI.Texture.fromImage('public_html/images/tryagainbtn.jpeg');

    var playAgainButton = new PIXI.Sprite(texturePlayAgainBtn);

    playAgainButton=setPosition(playAgainButton,2,20);
    playAgainButton=reSetAnchor(playAgainButton);
      playAgainButton.scale.set(1);

    playAgainButton.interactive = true;
    playAgainButton.buttonMode = true;
    LostScreenPanel.addChild(playAgainButton);

    playAgainButton.on('pointerdown', onButtonDown3);

   function onButtonDown3() {
    this.isdown = true;
      this.texture = texturePlayAgainButtonDown;
    this.alpha = 1;
      app.stage.addChild(thirdPage());
     }
     //End play Again Button
 animatedBlast2 = new PIXI.AnimatedSprite(blastAnimation.animations["blast"]);
     animatedBlast2=setPosition(animatedBlast2,2,2);
    animatedBlast2.scale.set(2);
    animatedBlast2.animationSpeed = 0.5;
    animatedBlast2.loop=true;
    animatedBlast2.play();
    animatedBlast2.visible=true;
    lostscreen.addChild(animatedBlast2);


   var secondPagestump1Bottom = new Sprite(resources["public_html/images/stumps.png"].texture);
 secondPagestump1Bottom=reSetAnchor(secondPagestump1Bottom);
 secondPagestump1Bottom=setPosition(secondPagestump1Bottom,2,235);
 secondPagestump1Bottom.scale.set(0.6);
  victoryScreen.addChild(secondPagestump1Bottom);
        //volume and music buttons start
  var victoryScrnVoltextureButton = resources["public_html/images/Buttons/SoundOnButton.png"].texture;
   var victoryScrnVoltextureButtonDown = resources["public_html/images/Buttons/SoundOnButtonDown.png"].texture;
   var victoryScrnVoltextureButtonOver = resources["public_html/images/Buttons/SoundOnButtonHover.png"].texture;

    var victoryScrnVolbutton = new PIXI.Sprite(victoryScrnVoltextureButton);
      victoryScrnVolbutton=setPosition(victoryScrnVolbutton,150,250);
     victoryScrnVolbutton.scale.set(0.2);

    victoryScrnVolbutton=reSetAnchor(victoryScrnVolbutton);
    victoryScrnVolbutton.interactive = true;
    victoryScrnVolbutton.buttonMode = true;
    victoryScreen.addChild(victoryScrnVolbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    victoryScrnVolbutton.on('pointerdown', victoryScrnVolonButtonDown);
    victoryScrnVolbutton.on('pointerup', victoryScrnVolonButtonUp);
    victoryScrnVolbutton.on('pointerover', victoryScrnVolonButtonOver);
    victoryScrnVolbutton.on('pointerout', victoryScrnVolonButtonOut);
    victoryScrnVolbutton.on('mouseover',victoryScrnVolonButtonOver);
    console.log(victoryExplodeAnimation.animations);
    animatedVictoryExplosion = new PIXI.AnimatedSprite(victoryExplodeAnimation.animations["Explosion 08"]);
     animatedVictoryExplosion=setPosition(animatedVictoryExplosion,200,150);
     animatedVictoryExplosion=reSetAnchor(animatedVictoryExplosion);
    animatedVictoryExplosion.scale.set(1.2);
    animatedVictoryExplosion.animationSpeed = 0.5;
      animatedVictoryExplosion.loop=false;
    animatedVictoryExplosion.play();
    animatedVictoryExplosion.visible=true;
     app.stage.addChild(animatedVictoryExplosion);
   function victoryScrnVolonButtonDown() {
    this.isdown = true;
      this.texture = victoryScrnVoltextureButtonDown;
    this.alpha = 1;
    victoryScreen.addChild(SoundOffBtn());
}
     function victoryScrnVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = victoryScrnVoltextureButtonOver;
    }
    else {
        this.texture = victoryScrnVoltextureButton;
    }
}

function victoryScrnVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = victoryScrnVoltextureButtonOver;
}

function victoryScrnVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = victoryScrnVoltextureButton;
}

var victoryScrnMusictextureButton = resources["public_html/images/Buttons/MusicOnButton.png"].texture;
   var victoryScrnMusictextureButtonDown = resources["public_html/images/Buttons/MusicOnButtonDown.png"].texture;
   var victoryScrnMusictextureButtonOver = resources["public_html/images/Buttons/MusicOnButtonHover.png"].texture;

    var victoryScrnMusicbutton = new PIXI.Sprite(victoryScrnMusictextureButton);
      victoryScrnMusicbutton=setPosition(victoryScrnMusicbutton,90,250);
 victoryScrnMusicbutton.scale.set(0.2);

    victoryScrnMusicbutton=reSetAnchor(victoryScrnMusicbutton);
    victoryScrnMusicbutton.interactive = true;
    victoryScrnMusicbutton.buttonMode = true;
    victoryScreen.addChild(victoryScrnMusicbutton);

    //Start Button Events
    victoryScrnMusicbutton.on('pointerdown', victoryScrnMusiconButtonDown);
    victoryScrnMusicbutton.on('pointerup', victoryScrnMusiconButtonUp);
    victoryScrnMusicbutton.on('pointerover', victoryScrnMusiconButtonOver);
    victoryScrnMusicbutton.on('pointerout', victoryScrnMusiconButtonOut);
    victoryScrnMusicbutton.on('mouseover', victoryScrnMusiconButtonOver);

   function victoryScrnMusiconButtonDown() {
    this.isdown = true;
      this.texture = victoryScrnMusictextureButtonDown;
    this.alpha = 1;
    victoryScreen.addChild(MusicOffBtn());
}
     function victoryScrnMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = victoryScrnMusictextureButtonOver;
    }
    else {
        this.texture = victoryScrnMusictextureButton;
    }
}

function victoryScrnMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = victoryScrnMusictextureButtonOver;
}

function victoryScrnMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = victoryScrnMusictextureButton;
}

 //*************Volume and Music OFF Buttons start
function SoundOffBtn(){
var thirdsceneVoltextureButton = resources["public_html/images/Buttons/SoundOffButton.png"].texture;
   var thirdsceneVoltextureButtonDown = resources["public_html/images/Buttons/SoundOffButtonDown.png"].texture;
   var thirdsceneVoltextureButtonOver = resources["public_html/images/Buttons/SoundOffButtonHover.png"].texture;

    var thirdsceneSoundOffBtn = new PIXI.Sprite(thirdsceneVoltextureButton);
      thirdsceneSoundOffBtn=setPosition(thirdsceneSoundOffBtn,150,250);
 thirdsceneSoundOffBtn.scale.set(0.2);

    thirdsceneSoundOffBtn=reSetAnchor(thirdsceneSoundOffBtn);
    thirdsceneSoundOffBtn.interactive = true;
    thirdsceneSoundOffBtn.buttonMode = true;
    victoryScreen.addChild(thirdsceneSoundOffBtn);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneSoundOffBtn.on('pointerdown', thirdsceneVolonButtonDown);
    thirdsceneSoundOffBtn.on('pointerup', thirdsceneVolonButtonUp);
    thirdsceneSoundOffBtn.on('pointerover', thirdsceneVolonButtonOver);
    thirdsceneSoundOffBtn.on('pointerout', thirdsceneVolonButtonOut);
    thirdsceneSoundOffBtn.on('mouseover', thirdsceneVolonButtonOver);

   function thirdsceneVolonButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneVoltextureButtonDown;
    this.alpha = 1;
    victoryScreen.removeChild(thirdsceneSoundOffBtn);

}
     function thirdsceneVolonButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneVoltextureButtonOver;
    }
    else {
        this.texture = thirdsceneVoltextureButton;
    }
}

function thirdsceneVolonButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButtonOver;
}

function thirdsceneVolonButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneVoltextureButton;
}
}
function MusicOffBtn(){
var thirdsceneMusictextureButton = resources["public_html/images/Buttons/MusicOffButton.png"].texture;
   var thirdsceneMusictextureButtonDown = resources["public_html/images/Buttons/MusicOffButtonDown.png"].texture;
   var thirdsceneMusictextureButtonOver = resources["public_html/images/Buttons/MusicOffButtonHover.png"].texture;

    var thirdsceneMusicbutton = new PIXI.Sprite(thirdsceneMusictextureButton);
      thirdsceneMusicbutton=setPosition(thirdsceneMusicbutton,90,250);
 thirdsceneMusicbutton.scale.set(0.2);

    thirdsceneMusicbutton=reSetAnchor(thirdsceneMusicbutton);
    thirdsceneMusicbutton.interactive = true;
    thirdsceneMusicbutton.buttonMode = true;
    victoryScreen.addChild(thirdsceneMusicbutton);
    //End button image ,position,scale,intractive and mode.

    //Start Button Events
    thirdsceneMusicbutton.on('pointerdown', thirdsceneMusiconButtonDown);
    thirdsceneMusicbutton.on('pointerup', thirdsceneMusiconButtonUp);
    thirdsceneMusicbutton.on('pointerover', thirdsceneMusiconButtonOver);
    thirdsceneMusicbutton.on('pointerout', thirdsceneMusiconButtonOut);
    thirdsceneMusicbutton.on('mouseover', thirdsceneMusiconButtonOver);

   function thirdsceneMusiconButtonDown() {
    this.isdown = true;
      this.texture = thirdsceneMusictextureButtonDown;
    this.alpha = 1;
        victoryScreen.removeChild(thirdsceneMusicbutton);
      gameSound.play();

}
     function thirdsceneMusiconButtonUp() {
    this.isdown = false;
    if (this.isOver) {
        this.texture = thirdsceneMusictextureButtonOver;
    }
    else {
        this.texture = thirdsceneMusictextureButton;
    }
}

function thirdsceneMusiconButtonOver() {
    this.isOver = true;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButtonOver;
}

function thirdsceneMusiconButtonOut() {
    this.isOver = false;
    if (this.isdown) {
        return;
    }
    this.texture = thirdsceneMusictextureButton;
}
      gameSound.pause();

}
 }


function reSetAnchor(sprite){

  sprite.anchor.x = 0.5;
  sprite.anchor.y = 0.5;
  return sprite;
}

function setPosition(sprite,x,y){
  sprite.x = x;
    sprite.y = y;

    return sprite;
}


//          PIXI.loader.add('loop2', 'public_html/loop2.mp3');
//            PIXI.loader.load(function(loader, resources) {
//        const sound = resources.loop2.sound || resources.loop2.data;
//        sound.loop = true; // Loop in case of music.
//
//        const button = document.getElementById('test-button');
//
//        button.addEventListener('mousedown', function() {
//            const button = this;
//
//            if(sound.isPlaying) {
//                sound.stop();
//            } else {
//                sound.play();
//            }
//
//            button.innerHTML = sound.isPlaying ? 'Stop' : 'Play';
//        });
//    });
//
//    document.querySelector("#paused").addEventListener('click', function() {
//    const paused = PIXI.sound.togglePauseAll();
//    this.className = this.className.replace(/\b(on|off)/g, '');
//    this.className += paused ? 'on' : 'off';
//});
   </script>
</body>
</html>-->


